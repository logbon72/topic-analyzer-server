/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author intelWorX
 */
public class Utils {

    public static String getRandomCode(int length) {
        return getRandomCode(length, false);
    }

    public static String getRandomCode(int length, boolean alphaNumeric) {
        String charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890$_|.";
        int strLen = alphaNumeric ? (charSet.length() - 5) : (charSet.length() - 1);
        String rand = "";
        for (int i = 0; i < length; i++) {
            int pos = (int) Math.round(Math.random() * strLen);
            rand += charSet.charAt(pos);
        }

        return rand;

    }

    public static String getRandomCode() {
        return getRandomCode(16, false);
    }

    public static String formatDate(Date ts) {
        return formatDate(ts, "MM/dd/yyyy");
    }

    public static String formatDate(Date ts, String format) {
        if (ts == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(ts);
    }

    public static String getFileExension(String fileName) {
        if (fileName == null) {
            return "";
        }
        String[] tmp = fileName.split("\\.");
        return tmp[tmp.length - 1];
    }

    public static String formatWeight(Double number) {
        return String.format("%5.2f", number);
    }

    public static String md5(String plaintext) {
        MessageDigest m;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            return null;
        }
        m.reset();
        m.update(plaintext.getBytes());
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String hashtext = bigInt.toString(16);
        // Now we need to zero pad it if you actually want the full 32 chars.
        while (hashtext.length() < 32) {
            hashtext = "0" + hashtext;
        }
        return hashtext.toLowerCase();
    }

    private static final ObjectMapper SORTED_MAPPER = new ObjectMapper();

    static {
        SORTED_MAPPER.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, false);
    }

    public static JsonNode convertNode(final JsonNode node)  {
        try {
            final JsonNode obj = SORTED_MAPPER.treeToValue(node, JsonNode.class);
            return obj;
            //final String json = SORTED_MAPPER.writeValueAsString(obj);
            //return json;
        } catch (JsonProcessingException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
//    public static ObjectNode newObject(java.util.Map<String, JsonNode> maps) {
//        ObjectMapper objectMapper = new ObjectMapper();
//        return objectMapper
//                .configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, false)
//                //.configure(Seria, true)
//                .valueToTree(maps);
//    }
}
