/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.entities;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import models.article.ExtractedArticle;
import play.Logger;
import play.db.ebean.Model;

/**
 *
 * @author intelWorX
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"url"}))
public class Article extends Model {

    public static Finder<Long, Article> finder = new Finder<Long, Article>(Long.class, Article.class);

    @Id
    protected Long id;
    protected String url;
    protected Boolean processed;

    protected String title;

    @Lob
    protected String content;

    @Column(length = 1024)
    protected String excerpt;

    @Temporal(TemporalType.TIMESTAMP)
    protected Date datePublished;

    protected String author;

    protected Timestamp createdOn = new Timestamp(new Date().getTime());

    @Temporal(TemporalType.TIMESTAMP)
    protected Timestamp processedOn;

    @OneToMany
    protected List<UserHistory> userHistorys;

    @OneToMany(cascade = CascadeType.ALL)
    protected List<ArticleTopic> topics;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the processed
     */
    public Boolean getProcessed() {
        return processed;
    }

    /**
     * @param processed the processed to set
     */
    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the excerpt
     */
    public String getExcerpt() {
        return excerpt;
    }

    /**
     * @param excerpt the excerpt to set
     */
    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    /**
     * @return the datePublished
     */
    public Date getDatePublished() {
        return datePublished;
    }

    /**
     * @param datePublished the datePublished to set
     */
    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the processedOn
     */
    public Timestamp getProcessedOn() {
        return processedOn;
    }

    /**
     * @param processedOn the processedOn to set
     */
    public void setProcessedOn(Timestamp processedOn) {
        this.processedOn = processedOn;
    }

    /**
     * @return the userHistorys
     */
    public List<UserHistory> getUserHistorys() {
        return userHistorys;
    }

    /**
     * @param userHistorys the userHistorys to set
     */
    public void setUserHistorys(List<UserHistory> userHistorys) {
        this.userHistorys = userHistorys;
    }

    public boolean updateWithArticle(ExtractedArticle article) {
        return updateWithArticle(article, true);
    }
    
    public boolean updateWithArticle(ExtractedArticle article, boolean save) {
        author = article.getAuthor();
        content = article.getContent();
        datePublished = article.getDatePublished();
        excerpt = article.getExcerpt();
        title = article.getTitle();
        if (save) {
            try {
                save();
                return true;
            } catch (OptimisticLockException ole) {
                Logger.warn("Save failed: " + ole.getLocalizedMessage());
                return false;
            }
        }
        return false;
    }

    public List<ArticleTopic> getTopics() {
        return topics;
    }

    public void setTopics(List<ArticleTopic> topics) {
        this.topics = topics;
    }

    /**
     * Retrieves article with this URL or creates a new article with this URL
     *
     * @param url
     * @return Article
     */
    public static Article fromUrl(String url) {
        Article article = finder.where()
                .ieq("url", url)
                .setMaxRows(1)
                .findUnique();

        if (article == null) {
            article = new Article();
            article.setUrl(url);
            article.setProcessed(false);
            article.save();
        }

        return article;
    }

}
