/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.entities;

import com.avaje.ebean.Ebean;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.*;
import models.Utils;
import models.UserHistoryForm;
import models.article.Processor;
import play.db.ebean.Model;

/**
 *
 * @author intelWorX
 */
@Entity
@Table(name = "user_t")
public class User extends Model {

    @Id
    protected Long id;
    protected String secretKey;
    protected Timestamp createdOn = new Timestamp(System.currentTimeMillis());
    public static Finder<Long, User> finder = new Finder<Long, User>(Long.class, User.class);

    @OneToMany
    @JoinColumn(name = "user_id")
    protected List<UserHistory> historys;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the secretKey
     */
    public String getSecretKey() {
        return secretKey;
    }

    /**
     * @param secretKey the secretKey to set
     */
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    /**
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the historys
     */
    public List<UserHistory> getHistorys() {
        return historys;
    }

    /**
     * @param historys the historys to set
     */
    public void setHistorys(List<UserHistory> historys) {
        this.historys = historys;
    }

    public static User register() {
        User user = new User();
        user.setId(System.currentTimeMillis());
        user.setSecretKey(Utils.getRandomCode(24));
        try {
            user.save();
            return user;
        } catch (OptimisticLockException ole) {
            play.Logger.warn("Could not save user" + ole.getMessage());
            return null;
        }
    }

    
    public boolean addHistory(UserHistoryForm userHistoryForm){
        
        for(UserHistoryForm.HistoryEntry he : userHistoryForm.getHistorys().values()){
            historys.add(UserHistory.fromHistoryEntry(he, this));
        }
        
        try{
            Ebean.save(historys);
            Processor.getInstance().newArticlesReceived();
            return true;
        }catch(OptimisticLockException ole){
            play.Logger.warn("Could not save the history: "+ole.getLocalizedMessage());
            return false;
        }
        
    }
}
