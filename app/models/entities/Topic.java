/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.entities;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import play.db.ebean.Model;

/**
 *
 * @author intelWorX
 */
@Entity
public class Topic extends Model {

    @Id
    protected Long id;
    @Column(length = 1024)
    protected String topic;
    protected Double score;
    protected Timestamp createdOn = new Timestamp(new Date().getTime());

    @OneToMany
    @JoinColumn(name = "topic_id")
    protected List<TopicWord> words;

    @OneToMany(cascade = CascadeType.ALL)
    protected List<ArticleTopic> articleTopics;

    public static Finder<Long, Topic> finder = new Finder<Long, Topic>(Long.class, Topic.class);

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     * @param topic the topic to set
     */
    public void setTopic(String topic) {
        this.topic = topic;
    }

    /**
     * @return the score
     */
    public Double getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(Double score) {
        this.score = score;
    }

    /**
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the words
     */
    public List<TopicWord> getWords() {
        return words;
    }

    /**
     * @param words the words to set
     */
    public void setWords(List<TopicWord> words) {
        this.words = words;
    }

}
