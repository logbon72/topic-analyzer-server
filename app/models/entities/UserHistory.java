/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.entities;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.persistence.*;
import models.UserHistoryForm;
import play.Play;
import play.libs.Json;

/**
 *
 * @author intelWorX
 */
@Entity
@Table(name = "user_history_t")
public class UserHistory {

    public static final long DEFAULT_DAYS_BACK = 7 * 86400000;
    public static final String OTHERS_ID = "others";

    @Id
    protected Long id;

    @Temporal(TemporalType.TIMESTAMP)
    protected Timestamp visitDate;

    protected Timestamp createdOn = new Timestamp(new Date().getTime());
    @ManyToOne(optional = false, targetEntity = User.class)
    @JoinColumn(name = "user_id")
    protected User user;
    @ManyToOne
    @JoinColumn(name = "article_id")
    protected Article article;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the visitDate
     */
    public Timestamp getVisitDate() {
        return visitDate;
    }

    /**
     * @param visitDate the visitDate to set
     */
    public void setVisitDate(Timestamp visitDate) {
        this.visitDate = visitDate;
    }

    /**
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return the article
     */
    public Article getArticle() {
        return article;
    }

    /**
     * @param article the article to set
     */
    public void setArticle(Article article) {
        this.article = article;
    }

    public static UserHistory fromHistoryEntry(UserHistoryForm.HistoryEntry he, User user) {
        UserHistory history = new UserHistory();
        history.setVisitDate(new Timestamp(he.getDate()));
        history.setUser(user);
        String url = UserHistoryForm.HistoryEntry.clean(he.getUrl());
        history.setArticle(Article.fromUrl(url));
        return history;
    }

    public static HashMap<String, JsonNode> getTopTopics(User user, Date startDateObj, Date endDateObj, ArrayNode treeMapData) {
        int limit = Play.application().configuration().getInt("report.topTopics");

        SqlQuery sqlQuery = Ebean.createNamedSqlQuery("UserHistory.topTopics");
        List<SqlRow> rows = sqlQuery
                .setParameter("startDate", startDateObj)
                .setParameter("endDate", endDateObj)
                .setParameter("userId", user.getId())
                .setParameter("limit", limit)
                .findList();

        HashMap<String, JsonNode> topics = new HashMap<String, JsonNode>(limit);

        int count = 0;
        ObjectNode wordNode;
        Topic topic;
        for (SqlRow row : rows) {
            if (count++ < limit) {
                topics.put(row.getString("topicId"), Json.newObject().textNode(row.getString("topic")));
            }
            wordNode = Json.newObject();
            wordNode.put("topic", row.getString("topic"));
            wordNode.put("weight", row.getString("totalScore"));
            topic = Topic.finder.byId(row.getLong("topicId"));
            if (topic != null) {//can't be too safe
                ArrayNode topicWordList = wordNode.arrayNode();
                for (TopicWord topicWord : topic.getWords()) {
                    ObjectNode sWordNode = Json.newObject();
                    sWordNode.put("word", topicWord.getWord());
                    sWordNode.put("weight", topicWord.getScore());
                    topicWordList.add(sWordNode);
                }
                wordNode.put("words", topicWordList);
            }
            treeMapData.add(wordNode);
        }

        topics.put(OTHERS_ID, Json.newObject().textNode("Other topics"));

        return topics;
    }

    public static JsonNode getReport(User user, String startDate, String endDate, String groupBy) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String pattern = "^\\d{4,4}-\\d{2,2}-\\d{2,2}$";
        //check start date
        Date startDateObj = new Date(System.currentTimeMillis() - DEFAULT_DAYS_BACK);
        if (startDate != null && startDate.length() > 0 && startDate.matches(pattern)) {
            try {
                startDateObj = sdf.parse(startDate);
            } catch (ParseException ex) {

            }
        }

        //check end date
        Date endDateObj = new Date();
        if (endDate != null && endDate.length() > 0 && endDate.matches(pattern)) {
            try {
                sdf.parse(endDate);
            } catch (ParseException pe) {
            }
        }
        //Calendar.getInstance().setTime(endDateObj);

        //check groupBy default to DAY
        ReportGroupingOption reportGroupingOption;
        if (groupBy == null || groupBy.length() < 1) {
            reportGroupingOption = ReportGroupingOption.DEFAULT;
        } else {
            groupBy = groupBy.trim().toUpperCase();
            try {
                reportGroupingOption = ReportGroupingOption.valueOf(groupBy);
            } catch (IllegalArgumentException iae) {
                reportGroupingOption = ReportGroupingOption.DEFAULT;
            }
        }

        SqlQuery sqlQuery = Ebean.createNamedSqlQuery("UserHistory.getReport");
        List<SqlRow> rows = sqlQuery.setParameter("format", reportGroupingOption.getFormat())
                .setParameter("orderFormat", reportGroupingOption.getOrderFormat())
                .setParameter("startDate", startDateObj)
                .setParameter("endDate", endDateObj)
                .setParameter("userId", user.getId())
                .findList();

        ArrayNode words = Json.newObject().arrayNode();
        HashMap<String, JsonNode> topTopics = getTopTopics(user, startDateObj, endDateObj, words);
        ObjectNode reportNode = formatSqlRows(rows, topTopics, reportGroupingOption);
        reportNode.put("words", words);

        return reportNode;
    }

    public static JsonNode getTopTopicWords(User user, Date startDate, Date endDate) {
        int limit = Play.application().configuration().getInt("report.topTopicWords");

        SqlQuery sqlQuery = Ebean.createNamedSqlQuery("UserHistory.topTopicWords");
        List<SqlRow> rows = sqlQuery
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .setParameter("userId", user.getId())
                .setParameter("limit", limit)
                .findList();

        return Json.toJson(rows);
    }

    private static JsonNode createGroupEntry(Collection<String> topTopics) {
        Map<String, JsonNode> groupEntry = new TreeMap<String, JsonNode>();
        for (String topicId : topTopics) {
            groupEntry.put(topicId, blankEntryNode());
        }
//        groupEntry.put(OTHERS_ID, blankEntryNode());

        return Json.newObject().putAll(groupEntry);
        //return groupEntry;
    }

    private static ObjectNode blankEntryNode() {
        ObjectNode blankNode = Json.newObject();
        //othersNode.put("topic", topic);
        blankNode.put("historyCount", 0);
        blankNode.put("totalScore", 0.0);
        blankNode.put("examples", "");
        return blankNode;
    }

    private static ObjectNode formatSqlRows(List<SqlRow> rows, HashMap<String, JsonNode> topTopics, ReportGroupingOption reportGroupingOption) {

        //play.Logger.info("Sorter: " + reportGroupingOption.getSorter());
        Map<String, JsonNode> formattedResult = new TreeMap<String, JsonNode>(reportGroupingOption.getSorter());

        ObjectNode topicNode;
        String topicId;
        for (SqlRow row : rows) {
            String formattedDate = row.getString("formattedDate");
            //create blank entry for group
            if (formattedResult.get(formattedDate) == null) {
                formattedResult.put(formattedDate, createGroupEntry(topTopics.keySet()));
            }

            topicId = row.getString("topicId");
            if (formattedResult.get(formattedDate).has(topicId)) {
                topicNode = (ObjectNode) formattedResult.get(formattedDate).get(topicId);
                //topicNode.put("topic", row.getString("topic"));
                topicNode.put("historyCount", row.getInteger("historyCount"));
                topicNode.put("totalScore", row.getDouble("totalScore"));
                topicNode.put("examples", row.getString("examples"));
            } else {
                topicNode = (ObjectNode) formattedResult.get(formattedDate).get(OTHERS_ID);
                topicNode.put("historyCount", row.getInteger("historyCount") + topicNode.get("historyCount").asInt());
                topicNode.put("totalScore", row.getDouble("totalScore") + topicNode.get("totalScore").asDouble());
                topicNode.put("examples", row.getString("examples") + "\n" + topicNode.get("examples").asText());
            }
        }

        ObjectNode reportNode = Json.newObject();
        reportNode.put("topics", Json.newObject().putAll(topTopics));
        JsonNode jsonNode = Json.newObject().putAll(formattedResult);
        reportNode.put("report", jsonNode);
        return reportNode;
    }

    public static class FormattedDateComparer implements Comparator<String> {

        public SimpleDateFormat inputFormat;
        public SimpleDateFormat outputFormat;

        public FormattedDateComparer(String inputFormat, String outputFormat) {
            this.inputFormat = new SimpleDateFormat(inputFormat);
            this.outputFormat = new SimpleDateFormat(outputFormat);
        }

        private Integer formatVal(String val) {
            try {
                return Integer.parseInt(outputFormat.format(inputFormat.parse(val)));
            } catch (ParseException ex) {
                play.Logger.error("Date formatting error: " + ex.getMessage());
                return 0;
            }
        }

        public int compare(String o1, String o2) {
            if (o1.equalsIgnoreCase(o2)) {
                return 0;
            }
            //play.Logger.info("O1: "+o1+"->"+formatVal(o1));
            //play.Logger.info("O2: "+o2+"->"+formatVal(o2));

            return formatVal(o1).compareTo(formatVal(o2));
            //return (o1).compareTo(o2);
        }

    }

    public static enum ReportGroupingOption {

        DAY("%Y-%m-%d"),
        WEEKDAY("%W", "%w", new FormattedDateComparer("EEEE", "u")),
        MONTH("%M", "%m", new FormattedDateComparer("MMMM", "MM")),
        WEEK("%Y,%V", "%Y-%m-%d"),;
        private final String format;
        private final String orderFormat;
        private final Comparator<String> sorter;

        public static final ReportGroupingOption DEFAULT = DAY;

        private ReportGroupingOption(String format, String orderFormat, Comparator<String> sorter) {
            this.format = format;
            this.orderFormat = orderFormat;
            this.sorter = sorter;
        }

        private ReportGroupingOption(String format) {
            this(format, format);
        }

        private ReportGroupingOption(String format, String orderFormat) {
            this(format, orderFormat, new Comparator<String>() {
                public int compare(String o1, String o2) {
                    return o1.compareTo(o2);
                }
            });
        }

        public Comparator<String> getSorter() {
            return sorter;
        }

        public String getFormat() {
            return format;
        }

        public String getOrderFormat() {
            return orderFormat;
        }

    }
}
