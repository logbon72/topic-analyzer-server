/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.entities;

import javax.persistence.*;

/**
 *
 * @author intelWorX
 */
@Entity
public class TopicWord {

    @Id
    protected Long id;
    @Column(length = 255)
    
    protected String word;
    protected Double score;
    
    @ManyToOne
    @JoinColumn(name = "topic_id")
    protected Topic articleTopic;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }

    /**
     * @return the score
     */
    public Double getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(Double score) {
        this.score = score;
    }

    /**
     * @return the articleTopic
     */
    public Topic getTopic() {
        return articleTopic;
    }

    /**
     * @param articleTopic the articleTopic to set
     */
    public void setTopic(Topic articleTopic) {
        this.articleTopic = articleTopic;
    }
    
    
    
}
