/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.article.topic;

import java.util.List;
import models.entities.Article;

/**
 *
 * @author intelWorX
 */
public interface AnalyzerService {

    public boolean processTopics(List<Article> articles);
    
}
