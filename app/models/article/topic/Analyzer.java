/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.article.topic;

import java.util.List;
import models.article.topic.mallet.MalletService;
import models.entities.Article;

/**
 *
 * @author intelWorX
 */
public class Analyzer {

    protected AnalyzerService analyzerService;

    public Analyzer() {
        analyzerService = new MalletService();
    }

    public Analyzer(AnalyzerService analyzerService) {
        this.analyzerService = analyzerService;
    }

    public boolean analyze(List<Article> articles) {
        return analyzerService.processTopics(articles);
    }
}
