/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.article.topic.mallet;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import play.Configuration;
import play.Play;
import play.Logger;

/**
 *
 * @author intelWorX
 */
public class ScriptProcess {

    public static final int ARGS = 7;
    public static final Configuration CONFIG = Play.application().configuration();
    public static final String SCRIPT = CONFIG.getString("analyzer.mallet.perlScript");
    public static final String INFERENCER = CONFIG.getString("analyzer.mallet.inferencer");
    public static final int MAX_TOPICS = CONFIG.getInt("analyzer.mallet.maxTopics");
    public static final String MALLET_BIN_DIR = System.getenv("MALLET_HOME") + System.getProperty("file.separator") + "bin";

    public final static File modelDir = new File(CONFIG.getString("analyzer.mallet.topicmodels"));
    public final static String modelBaseDir = modelDir.getAbsolutePath() + System.getProperty("file.separator");
    public final static String TOPICS_FILE = modelDir.getAbsolutePath() + System.getProperty("file.separator") + CONFIG.getString("analyzer.mallet.topics");
    public final static String TOPIC_WORDS_FILE = modelDir.getAbsolutePath()
            + System.getProperty("file.separator") + CONFIG.getString("analyzer.mallet.topicWords");
    public static final String TRAINING_MODEL = modelDir.getAbsolutePath()
            + System.getProperty("file.separator") + CONFIG.getString("analyzer.mallet.training");

    public final static int OUTPUT_LINES_SKIP = 1;

    /**
     * Ensure that the model directory is valid.
     */
    static {
        if (!modelDir.exists()) {
            throw new RuntimeException("Topic models directory was not found.");
        }
    }

    protected File outputFile;
    protected File inputDir;
    //protected ProcessBuilder processBuilder;
    protected int totalOutputLines;

    public ScriptProcess(File inputDir) throws IOException {

        if (!(inputDir.exists() && inputDir.canRead() && inputDir.isDirectory())) {
            throw new IllegalArgumentException("The specificed path: " + inputDir.getAbsolutePath() + " is either not readable, not a directory or does not exist.");
        }

        this.inputDir = inputDir;
        outputFile = File.createTempFile("topicModel_" + System.currentTimeMillis(), "output");

    }

    /**
     * Builds arguments for the calling of the script.
     *
     * @return
     */
    protected ProcessBuilder buildProcess() {
        ArrayList<String> commands = new ArrayList<String>();
        commands.add(CONFIG.getString("analyzer.mallet.perlExec"));
        commands.add(modelBaseDir + SCRIPT);
        commands.add("INPUTDIR=" + inputDir.getAbsolutePath());
        commands.add("INFERENCER=" + modelBaseDir + INFERENCER);
        commands.add("MALLETDIR=" + MALLET_BIN_DIR);
        commands.add("MAXTOPICS=" + MAX_TOPICS);
        commands.add("TRAINFILE=" + TRAINING_MODEL);
        commands.add("OUTPUTFILENAME=" + outputFile.getAbsolutePath());

        play.Logger.info("Commands:" + commands);
        ProcessBuilder processBuilder = new ProcessBuilder(commands);
        processBuilder.redirectErrorStream();
        return processBuilder;
    }

    /**
     *
     * @return a hash map of Filename, Topic score for each input file in the
     * input
     * @throws java.io.IOException
     */
    public HashMap<String, TopicClassification> getResult() throws IOException {
        //get output from perl process and pass to scanner
        Scanner opScanner = new Scanner(getRawOutput());

        int currentLine = 0;

        //the holder of result, expected capacity is the jumber of lines in output less number of lines
        //the script is expected to skip
        HashMap<String, TopicClassification> result = new HashMap<String, TopicClassification>(Math.max(totalOutputLines - OUTPUT_LINES_SKIP, 1));

        String line;
        String[] lineParts;

        while (opScanner.hasNextLine()) {
            line = opScanner.nextLine().trim();
            if (++currentLine > OUTPUT_LINES_SKIP && line.length() > 0) {
                lineParts = line.split("\\s+");
                TopicClassification topicsClassification = new TopicClassification(MAX_TOPICS);
                for (int i = 2; i + 1 < lineParts.length; i += 2) {
                    //convert pairs of topic id score to map.
                    topicsClassification.put(Integer.parseInt(lineParts[i]), Double.parseDouble(lineParts[i + 1]));
                }
                result.put(new File(lineParts[1]).getName(), topicsClassification);
            }

        }

        return result;
    }

    /**
     * Get raw output from topic models script.
     *
     * @return
     * @throws IOException
     */
    public String getRawOutput() throws IOException {
        Process perlProcess = buildProcess().start();
        final Scanner inputReader = new Scanner(perlProcess.getInputStream());
        final StringBuilder outString = new StringBuilder();

        totalOutputLines = 0;

        while (inputReader.hasNextLine()) {
            outString.append(inputReader.nextLine()).append("\n");
            totalOutputLines++;
        }

        Logger.info("Total lines from output: " + totalOutputLines);
        //Logger.debug(outString.toString());
        return outString.toString();
    }

}
