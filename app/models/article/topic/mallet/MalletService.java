/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.article.topic.mallet;

import com.avaje.ebean.Ebean;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import javax.persistence.OptimisticLockException;
import models.Utils;
import models.article.topic.AnalyzerService;
import models.entities.Article;
import models.entities.ArticleTopic;
import models.entities.Topic;
import models.entities.TopicWord;
import play.Logger;

/**
 *
 * @author intelWorX
 */
public class MalletService implements AnalyzerService {

    final private File articlesDirectory;
    private boolean isDone = false;

    public MalletService() {
        //generate random path name, to avoid collision amongst threads
        String path = System.getProperty("java.io.tmpdir")
                + System.getProperty("file.separator")
                + "articles_" + Utils.getRandomCode(12, true);

        articlesDirectory = new File(path);
        if (!articlesDirectory.mkdir()) {
            throw new RuntimeException("Temporary directory could not be created.");
        }

        Logger.info("Articles output directory: " + path);
    }

    public boolean processTopics(List<Article> articles) {
        boolean success = false;
        if (!isDone && !articles.isEmpty()) {
            try {

                String articleFile;
                HashMap<String, Article> articleFileMapping = new HashMap<String, Article>(articles.size());

                for (Article article : articles) {
                    articleFile = saveArticleToTextFile(article);
                    articleFileMapping.put(articleFile, article);
                    Logger.debug("Article saved to :" + articleFile);
                }

                ScriptProcess process = new ScriptProcess(articlesDirectory);
                HashMap<String, TopicClassification> analysisResult = process.getResult();
                setArticlesTopic(articleFileMapping, analysisResult);
                //clean up articles
                success = true;
            } catch (FileNotFoundException fnfe) {
                Logger.error("Could not save an article", fnfe);
            } catch (IOException ioe) {
                Logger.error("Could not process topics", ioe);
            } catch (RuntimeException re) {
                Logger.error("Could not process topics", re);
            }
        }

        isDone = true;
        cleanArticles();
        return success;
    }

    /**
     * Cleans articles that were saved to text file. And the directory they were
     * saved to.
     */
    private void cleanArticles() {
        int total = 0;
        if (articlesDirectory.exists()) {
            for (File file : articlesDirectory.listFiles()) {
                if (file.delete()) {
                    total++;
                }
            }

            Logger.debug("Files deleted: " + total);
            if (articlesDirectory.delete()) {
                Logger.debug("Directory has been deleted: " + articlesDirectory.getAbsolutePath());
            }
        }
    }

    private void setArticlesTopic(HashMap<String, Article> articleFileMapping, HashMap<String, TopicClassification> analysisResult) {

        TopicClassification classifications;
        Article article;
        ArticleTopic articleTopic;

        //loop through list of article file mappings
        for (String articleFile : articleFileMapping.keySet()) {
            classifications = analysisResult.get(articleFile);
            article = articleFileMapping.get(articleFile);
            if (classifications != null) {
                article.getTopics().clear();
                //add article topics
                for (int topicId : classifications.keySet()) {
                    Topic topic = Topic.finder.byId(malletTopicIdToId(topicId));
                    articleTopic = new ArticleTopic();
                    articleTopic.setTopic(topic);
                    articleTopic.setScore(classifications.get(topicId));
                    articleTopic.setArticle(article);
                    article.getTopics().add(articleTopic);

                }

                Ebean.save(article.getTopics());
            }
        }
    }

    private String saveArticleToTextFile(Article article) throws FileNotFoundException {
        File outputFile = new File(articlesDirectory.getAbsolutePath()
                + System.getProperty("file.separator")
                + "article."
                + article.getId() + ".txt");

        Formatter articleWritter = new Formatter(new FileOutputStream(outputFile));

        articleWritter.format("%s%n", article.getTitle());
        articleWritter.format("%n");
        articleWritter.format("%s%n", article.getContent());
        articleWritter.flush();
        articleWritter.close();
        return outputFile.getName();
    }

    /**
     * Convert mallet topic ID to ID of topic entity. The padding with 100000 is
     * just a fancy way to prevent the topic list from starting from 0;
     *
     * @param id
     * @return
     */
    public static long malletTopicIdToId(int id) {
        return 1000000L + id;
    }

    public static void loadAndSaveTopics() {

        File keysFile = new File(ScriptProcess.TOPICS_FILE);

        try {
            Scanner scanner = new Scanner(keysFile);
            String line;
            int loaded = 0;
            while (scanner.hasNextLine()) {
                line = scanner.nextLine().trim();
                if (line.length() > 0) {
                    Topic topic = new Topic();
                    String tmp[] = line.split("\\s+");
                    topic.setId(malletTopicIdToId(Integer.parseInt(tmp[0])));
                    topic.setScore(Double.parseDouble(tmp[1]));
                    StringBuilder topicBuilder = new StringBuilder();
                    for (int i = 2; i < tmp.length; i++) {
                        topicBuilder.append(tmp[i]);
                        if (i < tmp.length - 1) {
                            topicBuilder.append(", ");
                        }
                    }
                    topic.setTopic(topicBuilder.toString());
                    loaded++;
                    topic.save();
                }
            }

            Logger.info("total topics loaded: " + loaded);
            loadAndSaveTopicWords();
        } catch (FileNotFoundException ex) {
            Logger.error("Could not load topics", ex);
        }
    }

    public static void loadAndSaveTopicWords() {

        File wordsFile = new File(ScriptProcess.TOPIC_WORDS_FILE);
        try {
            Scanner scanner = new Scanner(wordsFile);
            String line;
            String[] lineTmp;
            int loaded = 0;
            List<TopicWord> topicWords = new ArrayList<TopicWord>();
            TopicWord topicWord;
            while (scanner.hasNextLine()) {
                line = scanner.nextLine().trim();
                if (line.length() > 0) {
                    lineTmp = line.split("\\s+");
                    if (lineTmp.length >= 3) {
                        topicWord = new TopicWord();
                        Topic topic = Topic.finder.byId(malletTopicIdToId(Integer.parseInt(lineTmp[0])));
                        if (topic != null) {
                            topicWord.setTopic(topic);
                            topicWord.setScore(Double.parseDouble(lineTmp[2]));
                            topicWord.setWord(lineTmp[1]);
                            topicWords.add(topicWord);
                            loaded++;
                        }
                    }
                }
            }
            //
            try {
                if (!topicWords.isEmpty()) {
                    Ebean.save(topicWords);
                }
                Logger.info("total topic words loaded: " + loaded);
            } catch (OptimisticLockException ole) {
                Logger.error("could not save words", ole);
            }
        } catch (FileNotFoundException ex) {
            Logger.error("Could not load topics", ex);
        }
    }

}
