/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package models.article.extractors;

import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.article.ExtractedArticle;
import models.article.Extractor;
import play.libs.Json;

/**
 *
 * @author intelWorX
 */
public class Readability implements Extractor {
    final private static Readability instance =new Readability();

    final private String token;
    final static private String URL_PARSER = "https://readability.com/api/content/v1/parser?url=_url_&token=_token_";
    final private int CONNECT_TIMEOUT = 30000;
    
    private Readability(){
        token = play.Play.application().configuration().getString("readability.api.token");
    }
    
    
    
    public static Readability getInstance() {
        return instance;
    }

    public ExtractedArticle extract(String url) {
        try {
            String parserUrl = URL_PARSER.replace("_token_", token).replace("_url_", URLEncoder.encode(url, "UTF-8") );
            URL urlToCall = new URL(parserUrl);
            HttpURLConnection http = (HttpURLConnection) urlToCall.openConnection();
            http.setConnectTimeout(CONNECT_TIMEOUT);
            JsonNode jsonNode =  Json.parse(http.getInputStream());
            
            return new ReadabilityArticle(jsonNode);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Readability.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MalformedURLException ex) {
            Logger.getLogger(Readability.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Readability.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    
    
}
