/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.article.extractors;

import com.fasterxml.jackson.databind.JsonNode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import models.article.ExtractedArticle;
import org.jsoup.Jsoup;

/**
 *
 * @author intelWorX
 */
public class ReadabilityArticle extends ExtractedArticle {

    public ReadabilityArticle(JsonNode jsonResponse) {
        title = jsonResponse.get("title").asText();
        author = jsonResponse.get("author").asText();
        excerpt = jsonResponse.get("excerpt").asText();
        String date = jsonResponse.get("date_published").asText();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            datePublished = simpleDateFormat.parse(date);
        } catch (ParseException ex) {
            datePublished = null;
        }

        String contentHtml = jsonResponse.get("content").asText();
        content = Jsoup.parse(contentHtml).text();
    }

}
