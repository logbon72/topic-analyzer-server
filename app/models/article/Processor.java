/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.article;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.ExpressionList;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import models.article.extractors.Readability;
import models.article.topic.Analyzer;
import models.entities.Article;

/**
 *
 * @author intelWorX
 */
public class Processor {

    /**
     * Maximum number of threads that can be run when processing the articles
     */
    final public static int MAX_THREADS = 10;

    final private static Processor instance = new Processor();
    protected Extractor articleExtractor;
    protected ArrayList<ArticleProcessTask> processingTasks = new ArrayList<ArticleProcessTask>();
    protected long lastId;
    /**
     * States if the processor received article requests while thread list was
     * full
     */
    private boolean waiting = false;

    private Processor() {
        articleExtractor = Readability.getInstance();
    }

    synchronized public void setWaiting(boolean b) {
        waiting = b;
    }

    public static Processor getInstance() {
        return instance;
    }

    public void setArticleExtractor(Extractor articleExtractor) {
        this.articleExtractor = articleExtractor;
    }

    public Extractor getArticleExtractor() {
        return articleExtractor;
    }

    public ArrayList<ArticleProcessTask> getProcessingTasks() {
        return processingTasks;
    }

    /**
     * called when new articles are received, it processes the articles,
     * extracts the contents from the URL and then saves it to the database
     *
     */
    public void newArticlesReceived() {
        if (processingTasks.size() < MAX_THREADS) {
            setWaiting(false);
            Thread articleUpdater = new Thread(new ArticleProcessTask());
            articleUpdater.start();
        } else {
            setWaiting(true);
        }
    }

    public class ArticleProcessTask implements Runnable {

        final private Analyzer topicAnalyzer;

        public ArticleProcessTask() {
            topicAnalyzer = new Analyzer();
        }

        public void run() {
            //add to list of processing tasks.
            processingTasks.add(this);
            ExpressionList<Article> articlesWhere = Article.finder.where();
            if (!processingTasks.isEmpty()) {
                articlesWhere = articlesWhere.gt("id", lastId);
            }

            List<Article> articles = articlesWhere.eq("processed", false)
                    .orderBy("id DESC")
                    .findList();

            if (!articles.isEmpty()) {
                lastId = articles.get(0).getId();
                for (Article article : articles) {
                    play.Logger.info("Processing: " + article.getUrl());

                    if (article.getContent() == null) {//only update article if it is blank
                        ExtractedArticle extractedArticle = articleExtractor.extract(article.getUrl());
                        if (extractedArticle != null) {
                            article.updateWithArticle(extractedArticle, true);
                        }
                    } else {
                        play.Logger.info("\tAlready Processed ");
                    }
                    article.setProcessed(true);
                    article.setProcessedOn(new Timestamp(new Date().getTime()));
                }

                if (topicAnalyzer.analyze(articles)) {
                    Ebean.save(articles);
                }

            }
            //remove thread from task list
            processingTasks.remove(this);
            if (waiting) {
                newArticlesReceived();//process pending calls
            }
        }

    }
}
