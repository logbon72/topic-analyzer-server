/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.HashMap;
import play.data.validation.Constraints.*;

/**
 *
 * @author intelWorX
 */
public class UserHistoryForm {

    protected HashMap<Integer, HistoryEntry> historys = new HashMap<Integer, HistoryEntry>();

    public void setHistorys(HashMap<Integer, HistoryEntry> historys) {
        this.historys = historys;
    }

    public HashMap<Integer, HistoryEntry> getHistorys() {
        return historys;
    }

    public String validate() {
        if (historys == null || historys.isEmpty()) {
            return "empty history list";
        }
        return null;
    }

    public static class HistoryEntry {

        @Required
        private long date;

        @Required
        private String url;

        public long getDate() {
            return date;
        }

        public String getUrl() {
            return url;
        }

        public void setDate(long date) {
            this.date = date;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String validate() {
            String regex = "\\b(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
            if (!url.matches(regex)) {
                return "Invalid URL provided";
            }
            url = clean(url);
            return null;
        }

        public static String clean(String url) {
//            try {
//                URL urlObj = new URL(url);
//                String query = urlObj.getQuery();
//                if(query != null && query.length() > 0){
//                    String[] queryVars = query.split("&");
//                    ArrayList<String> parts = new ArrayList<String>(queryVars.length);
//                    for(String queryPart : queryVars){
//                        
//                    }
//                }
//                return urlObj.toString();
//            } catch (MalformedURLException ex) {
//                return null;
//            }

            //this is a huge assumption, but will cover most cases
            //most times, query vars are used for tracking purposes only.
            //so it is "safe" to remove them
            return url.split("\\?")[0];
        }

    }
}
