package controllers;

//import play.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.mvc.*;
import java.io.IOException;
import models.UserHistoryForm;
import models.article.Processor;
import models.article.topic.mallet.MalletService;
import models.entities.Article;
import models.entities.Topic;
import models.entities.User;
import models.entities.UserHistory;
import org.apache.commons.codec.digest.DigestUtils;
import play.Play;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import views.html.index;

public class Application extends Controller {

   
    

    public static Result index() throws IOException {
        return ok(index.render("Content", "Content Again"));
    }

    public static Result register() {
        setHeaders();

        User newUser = User.register();
        if (newUser != null) {
            JsonNode resultNode = Json.toJson(newUser);
            return ok(resultNode);
        } else {
            ObjectNode oNode = Json.newObject();
            oNode.put("error", "Could not create user");
            return badRequest(oNode);
        }
    }

    public static Result submitHistory() {
        setHeaders();
        RequestValidator req = getAuth();
        if (!req.valid) {
            return error(req.error);
        }

        Form<UserHistoryForm> userHistoryForm = Form.form(UserHistoryForm.class).bindFromRequest();
        if (userHistoryForm.hasErrors()) {
            return badRequest(userHistoryForm.errorsAsJson());
        }

        UserHistoryForm validForm = userHistoryForm.get();
        if (req.currentUser != null && req.currentUser.addHistory(validForm)) {
            //just return 1, to show successful
            return ok("{\"total\": " + validForm.getHistorys().size() + "}");
        } else {
            return error("Could not save history");
        }

    }

    private static Result error(String error) {
        ObjectNode oNode = Json.newObject();
        oNode.put("error", error);
        return badRequest(oNode);
    }

    private static RequestValidator getAuth() {
        String token = request().getQueryString("token");
        if (token == null) {
            token = "";
        }

        boolean testMode = request().getQueryString("isTest") != null;

        return new RequestValidator(token, testMode);
    }

    public static Result report() {
        setHeaders();

        DynamicForm dynamicForm = Form.form().bindFromRequest();
        RequestValidator req = getAuth();
        if (!req.valid) {
            return error(req.error);
        }

        ObjectNode resultNode = Json.newObject();

        JsonNode topicsNode = UserHistory.getReport(req.currentUser, dynamicForm.get("startDate"), dynamicForm.get("endDate"), dynamicForm.get("groupBy"));
        resultNode.put("topics", topicsNode);

        return ok(resultNode);
    }

    public static Result preflight(String p) {         // Ensure this header is also allowed!  
        setHeaders();
        return ok();
    }

    public static Result c() {         // Ensure this header is also allowed! 
        Processor.getInstance().newArticlesReceived();
        return ok();
    }

    /**
     * Allows for
     */
    private static void setHeaders() {
        response().setHeader("Access-Control-Allow-Origin", "*");
        response().setHeader("Access-Control-Allow-Methods", "POST");
        response().setHeader("Access-Control-Max-Age", "300");
        response().setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Connection, Content-length");
        response().setContentType("application/json");
    }

    public static class RequestValidator {

        private User currentUser;
        private boolean valid = false;
        private String error;
        private final String TOKEN_SEPARATOR = "-";
        private final long ALLOWED_LAG = 60000;

        public RequestValidator(String token, boolean testMode) {
            String[] tokenParts = token.split(TOKEN_SEPARATOR);
            if (tokenParts.length >= 3) {
                long userId = Long.parseLong(tokenParts[0]);
                long ts = Long.parseLong(tokenParts[1]);
                String sign = tokenParts[2];
                valid = (validateUser(userId) && validateSignature(ts, sign));
            } else if (testMode && !Play.application().isProd()) {
                currentUser = User.finder.setMaxRows(1)
                        .findUnique();
                valid = true;
            } else {
                error = "Invalid token format";
            }
        }

        private boolean validateUser(long userId) {
            currentUser = User.finder.byId(userId);
            if (currentUser == null) {
                error = "Invalid User ID";
                return false;
            }
            return true;
        }

        private boolean validateSignature(long ts, String sign) {
            if (Math.abs(System.currentTimeMillis() - ts) > ALLOWED_LAG) {
                error = "Timestamp is not valid";
                return false;
            }

            if (!generateSignature(ts).equalsIgnoreCase(sign)) {
                error = "Invalid signature.";
                return false;
            }

            return true;
        }

        private String generateSignature(long ts) {
            String toHash = new StringBuilder()
                    .append(currentUser.getId())
                    .append(ts)
                    .append(currentUser.getSecretKey())
                    .toString();

            return DigestUtils.sha1Hex(toHash);
        }

        public String getError() {
            return error;
        }

        public boolean isValid() {
            return valid;
        }

    }

}
