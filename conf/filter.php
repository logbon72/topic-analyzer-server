<?php
/**
 * Usage :
 * php filter.php history_file.csv domains_to_include.txt outputfilepath.csv
 */

$inputFile = $argc > 1 ? $argv[1] : null;

if (!$inputFile || !file_exists($inputFile)) {
    die('No input file or input file not found.');
}

$domainListFile = $argc > 2 ? $argv[2] : null;
$domainList = array();
if ($domainListFile && file_exists($domainListFile)) {
    $domainList = file($domainListFile);
}

array_walk($domainList, function(&$domain) {
    $domain = strtolower(trim($domain));
});
$outFile = $argc > 3 ? $argv[3] : "output." . time() . ".history.csv";


//id,lastVisitTime,title,typedCount,url,visitCount
$csvFh = fopen($inputFile, 'r');
$currentLine = 0;
//$domains = array();
$foutFh = fopen($outFile, 'w+');
$written = 0;

while ($record = fgetcsv($csvFh)) {
    if (++$currentLine == 1) {
        continue;
    }

    $output = true;

    if (!empty($domainList)) {
        $host = parse_url($record[4], PHP_URL_HOST);
        $output = in_array($host, $domainList);
    }
   
    if ($output && strlen(trim(parse_url($record[4], PHP_URL_PATH), '/ '))) {
        //write
        fputcsv($foutFh, array($record[1], $record[4]));
        $written++;
    }
}

fclose($foutFh);
fclose($csvFh);

echo "Wrote, ", $written, " lines to ", $outFile, PHP_EOL;
