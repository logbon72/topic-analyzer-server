# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table article (
  id                        bigint auto_increment not null,
  url                       varchar(255),
  processed                 tinyint(1) default 0,
  title                     varchar(255),
  content                   longtext,
  excerpt                   varchar(1024),
  date_published            datetime,
  author                    varchar(255),
  created_on                datetime,
  processed_on              datetime,
  constraint uq_article_1 unique (url),
  constraint pk_article primary key (id))
;

create table article_topic (
  id                        bigint auto_increment not null,
  score                     double,
  created_on                datetime,
  article_id                bigint,
  topic_id                  bigint,
  constraint pk_article_topic primary key (id))
;

create table topic (
  id                        bigint auto_increment not null,
  topic                     varchar(1024),
  score                     double,
  created_on                datetime,
  constraint pk_topic primary key (id))
;

create table topic_word (
  id                        bigint auto_increment not null,
  word                      varchar(255),
  score                     double,
  topic_id                  bigint,
  constraint pk_topic_word primary key (id))
;

create table user_t (
  id                        bigint auto_increment not null,
  secret_key                varchar(255),
  created_on                datetime,
  constraint pk_user_t primary key (id))
;

create table user_history_t (
  id                        bigint auto_increment not null,
  visit_date                datetime,
  created_on                datetime,
  user_id                   bigint not null,
  article_id                bigint,
  constraint pk_user_history_t primary key (id))
;

alter table article_topic add constraint fk_article_topic_article_1 foreign key (article_id) references article (id) on delete restrict on update restrict;
create index ix_article_topic_article_1 on article_topic (article_id);
alter table article_topic add constraint fk_article_topic_topic_2 foreign key (topic_id) references topic (id) on delete restrict on update restrict;
create index ix_article_topic_topic_2 on article_topic (topic_id);
alter table topic_word add constraint fk_topic_word_articleTopic_3 foreign key (topic_id) references topic (id) on delete restrict on update restrict;
create index ix_topic_word_articleTopic_3 on topic_word (topic_id);
alter table user_history_t add constraint fk_user_history_t_user_4 foreign key (user_id) references user_t (id) on delete restrict on update restrict;
create index ix_user_history_t_user_4 on user_history_t (user_id);
alter table user_history_t add constraint fk_user_history_t_article_5 foreign key (article_id) references article (id) on delete restrict on update restrict;
create index ix_user_history_t_article_5 on user_history_t (article_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table article;

drop table article_topic;

drop table topic;

drop table topic_word;

drop table user_t;

drop table user_history_t;

SET FOREIGN_KEY_CHECKS=1;

